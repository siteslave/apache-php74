
FROM php:7.4-apache-bullseye

RUN apt-get update && apt-get install -y libfreetype6-dev libjpeg62-turbo-dev \
    && docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-install mysqli \
    && docker-php-ext-enable pdo_mysql mysqli gd

    
RUN a2enmod rewrite
